﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculadora : MonoBehaviour
{
    public static string textoAyuda = "HELP";

    private void MuestraAyuda(string texto)
    {
        Debug.Log(textoAyuda + ":" + texto);
    }

    public int Suma(int num1, int num2)
    {
        MuestraAyuda("Suma Entera");
        return num1 + num2;

    }

    public int Resta(int num1, int num2)
    {
        Debug.Log("Resta Entera");
        return num1 - num2;

    }

    public int Multiplicacion(int num1, int num2)
    {
        Debug.Log("Multiplicacion Entera");
        return num1 * num2;

    }

    public int Division(int num1, int num2)
    {
        Debug.Log("Division Entera");
        return num1 / num2;

    }

    public float Suma(float num1, float num2)
    {
        Debug.Log("Suma Floats");
        return num1 + num2;

    }

    public float Resta(float num1, float num2)
    {
        Debug.Log("Resta Floats");
        return num1 - num2;

    }

    public float Multiplicacion(float num1, float num2)
    {
        Debug.Log("Multiplicacion Floats");
        return num1 * num2;

    }

    public float Division(float num1, float num2)
    {
        Debug.Log("Division Floats");
        return num1 / num2;

    }
}
